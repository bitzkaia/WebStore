﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NewStore.Data.Migrations
{
    public partial class ComprasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "01356f30-4e47-4902-a732-a8bbfcbfe963", "7b293a7d-7024-4961-8467-e7c22003ba3a" });

            migrationBuilder.CreateTable(
                name: "Compras",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserID = table.Column<string>(nullable: true),
                    ProductID = table.Column<int>(nullable: false),
                    DtCompra = table.Column<DateTime>(nullable: false),
                    Finalized = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Compras", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "eb962ae1-e3d1-4aba-b256-43cdce7d97f4", "d3718868-b465-4e85-bb12-869d4917ab09", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Compras");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "eb962ae1-e3d1-4aba-b256-43cdce7d97f4", "d3718868-b465-4e85-bb12-869d4917ab09" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "01356f30-4e47-4902-a732-a8bbfcbfe963", "7b293a7d-7024-4961-8467-e7c22003ba3a", "Admin", "ADMIN" });
        }
    }
}
