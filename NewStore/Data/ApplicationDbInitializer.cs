﻿using Microsoft.AspNetCore.Identity;
using NewStore.Areas.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewStore.Data
{
    //Cria ususario administrador, se já não está criado
    public static class ApplicationDbInitializer
    {
        public static void SeedUsers(UserManager<AppUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@this.com").Result == null)
            {
                AppUser user = new AppUser
                {
                    UserName = "admin@this.com",
                    Email = "admin@this.com",
                    Name = "Administrator",
                    Cash = 0
                };

                IdentityResult result = userManager.CreateAsync(user, "NotSecure123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }
    }
}
