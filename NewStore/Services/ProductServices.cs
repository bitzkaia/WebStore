﻿using Microsoft.EntityFrameworkCore;
using NewStore.Data;
using NewStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewStore.Services
{
    public class ProductServices : IProductServices
    {
        private readonly ApplicationDbContext _context;
        public ProductServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Product[]> GetIncompleteProductAsync()
        {
            return await _context.Products.ToArrayAsync();
        }
    }
}
