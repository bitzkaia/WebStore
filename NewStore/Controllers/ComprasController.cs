﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewStore.Areas.Identity;
using NewStore.Data;
using NewStore.Models;

namespace NewStore.Controllers
{
    [Authorize]
    public class ComprasController : Controller
    {
        //Injeção de classe de BD
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;

        public ComprasController(ApplicationDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }        

        //Ao finalizar a operação de compra
        [HttpPost]
        public async Task<IActionResult> FinalizarCompra(CompraViewModel model)
        {
            var currentUser = await _userManager.GetUserAsync(User);

            float precoFinal = 0;            
            
            foreach (ProdutoCompraView pcv in model.ComprasProdutos)
            {
                //Soma os preçoc dos produtos do carrinho
                precoFinal += pcv.Produto.Price;
                var compraId = pcv.CompraProd.Id;
                var compra = await _context.Compras.FindAsync(compraId);

                //Atualiza os produtos do banco de dados para efetivar as compras
                compra.Finalized = true;
                compra.DtCompra = DateTime.Now;
                _context.Update(compra);
            }

            if(precoFinal > currentUser.Cash)
            {
                //Compra não autorizada, saldo insuficiente, Recarrega a página com mensagem de erro
                model.ErrorMessage = "Saldo insuficiente para finalizar a compra.";
                return View("CompraView", model);
            }
            else
            {
                //Compra aprovada, salva modificações do banco, abre tela de historico de compras
                currentUser.Cash -= precoFinal;
                await _userManager.UpdateAsync(currentUser);
                await _context.SaveChangesAsync();

                return RedirectToAction("Historico");
            }

        }
        
        public async Task<IActionResult> Historico()
        {
            var currentUser = await _userManager.GetUserAsync(User);

            //Carrega lista de compras que já foram aprovados na compra daquele cliente
            var comprasFinalizadas = await _context.Compras
               .Where(x => x.Finalized == true && x.UserID == currentUser.Id)
               .ToArrayAsync();

            List<ProdutoCompraView> prods = new List<ProdutoCompraView>();

            //Encontra produto correspondente ao codigo dele na compra
            foreach (Compra c in comprasFinalizadas)
            {
                var temp = await _context.Products
                    .Where(x => x.ProductID == c.ProductID)
                    .SingleOrDefaultAsync();
                prods.Add(
                    new ProdutoCompraView
                    {
                        Produto = temp,
                        CompraProd = c
                    });
            }

            //Monta obj para View
            var compraVM = new CompraViewModel
            {
                ComprasProdutos = prods,
                UserName = currentUser.Name
            };

            return View(compraVM);
        }

        //Tela de carrinho
        public async Task<IActionResult> CompraView(int? id)
        {
            var currentUser = await _userManager.GetUserAsync(User);

            //Se possuir id adiciona na lista de compras não finalizadas do cliente
            if (id != null)
            {

               var product = await _context.Products
                    .FirstOrDefaultAsync(m => m.ProductID == id);
                if (product == null)
                {
                    return NotFound();
                }
                await AddCompraCart(currentUser, product);
            }

            //Obtem lista de compras não finalizadas do cliente
            var comprasUserCart = await _context.Compras
                .Where(x => x.Finalized == false && x.UserID == currentUser.Id)
                .ToArrayAsync();

           List<ProdutoCompraView> prods = new List<ProdutoCompraView>();

            //Encontra produto correspondente ao codigo dele na compra
            foreach (Compra c in comprasUserCart)
            {
                var temp = await _context.Products
                    .Where(x => x.ProductID == c.ProductID)
                    .SingleOrDefaultAsync();
                prods.Add(
                    new ProdutoCompraView
                    {
                        Produto = temp,
                        CompraProd = c
                    });
            }

            //Monta obj para View
            var compraVM = new CompraViewModel
            {
                ComprasProdutos = prods,
                UserName = currentUser.Name
            };

            return View(compraVM);
        }

        public async Task<IActionResult> RemoveCompra(Guid id)
        {
           //Remove compra do carrinho
            var compra = await _context.Compras.FindAsync(id);
            _context.Compras.Remove(compra);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(CompraView));
            
        }

        //Adiciona compra no carrinho
        public async Task<bool> AddCompraCart(AppUser currentUser, Product prod)
        {
            Compra compraCart = new Compra
            {
                ProductID = prod.ProductID,
                UserID = currentUser.Id,
                Id = new Guid(),
                Finalized = false
            };
            _context.Compras.Add(compraCart);

            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1;
        }

    }
}