﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewStore.Areas;
using System.ComponentModel.DataAnnotations;

namespace NewStore.Models
{
    //Classe para BD
    public class Compra
    {
        [Key]
        public Guid Id { get; set; }

        public string UserID { get; set; }
    
        public int ProductID { get; set; }

        public DateTime DtCompra { get; set; }

        public bool Finalized { get; set; }
    }
}
