﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewStore.Models
{
    //Produto vinculado a compra para tela
    public class ProdutoCompraView
    {
        public Product Produto { get; set; }

        public Compra CompraProd { get; set; }
    }
}
