﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewStore.Models
{
    //Modelo para aparecer na listagem de produtos
    public class ProductViewModel
    {
        public Product[] Products{ get; set; }
    }
}
