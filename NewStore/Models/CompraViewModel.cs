﻿using NewStore.Areas.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewStore.Models
{
    //Modelo para as telas de listagem de compras
    public class CompraViewModel
    {
        public string UserName{ get; set; }
        public List<ProdutoCompraView> ComprasProdutos { get; set; }
        public string ErrorMessage { get; set; }
    }
}
